package com.b2b.ehubportal

class Enums {
    public enum httpMethod {
        GET, HEAD, POST, PUT, PATCH, DELETE, OPTIONS, TRACE
    }

    public enum apiAction {
        add, list, delete, update, search
    }

    public enum apiName {
        workflow, agreement, workflowprocess, doctype, mailbox, processor, sequencegenerator, mailboxehubclient,
        ehubclient, ehubclientconfig, adminsetting, clientidentification,document,documentcontentmapping,documentcontent,
        documentconfirmation, documentmonitoring, documenttracking, splitdocs, mergeddocs
    }

    public enum apiParam {
        db, offset, limit, filter, sort
    }

    public enum apiParamDb {
        ehub_aeon, ehub_general, ehub_guardian, ehub_sg, ehub_integration, ehub_general_dc, ehub_hero, ehub_giant
    }

    public enum apiLimit {

    }

    public enum workflowSorting {
        WORKFLOWID, SENDERMAILBOXID, RECEIVERMAILBOXID, DOCTYPEID
    }

    public enum workflowProcessSorting {
        WORKFLOWID, SEQUENCEORDER
    }

    public enum documentContentMappingSorting {
        VERSION, CONTENTID, DOCID
    }

    public enum documentSorting {
        SENDERMAILBOXID, CREATIONDATE, DOCID
    }
}

