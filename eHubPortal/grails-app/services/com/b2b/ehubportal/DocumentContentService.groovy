package com.b2b.ehubportal

import grails.config.Config
import grails.core.support.GrailsConfigurationAware
import org.apache.http.client.utils.URIBuilder

class DocumentContentService implements GrailsConfigurationAware {
    String apiUrl
    String apiVersion

    @Override
    void setConfiguration(Config co) {
        apiUrl = co.getProperty('api.url')
        apiVersion = co.getProperty('api.version')
    }

    def deleteDocumentContent(String db, String filter) {
        URIBuilder dcURL = new URIBuilder(apiUrl + "/" + Enums.apiName.documentcontent.toString() + "/" + apiVersion + "/" + Enums.apiAction.delete.toString())

        def params = APIService.buildParams(db, null, null, null, filter)
        dcURL.addParameters(params)
        def url = new URL(dcURL.build().toString())

        HttpURLConnection response = (HttpURLConnection) url.openConnection()
        response.setRequestMethod(Enums.httpMethod.GET.toString())
        // connection.setConnectTimeout(10000)
        response.connect()

        println '===Delete DC===' + response.responseCode

        if (response.responseCode == 200 || response.responseCode == 202) {
            return response
        } else {
        }
        null
    }

    def listDocumentContent() {

    }

    def searchDocumentContent() {

    }
}
