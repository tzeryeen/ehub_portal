package com.b2b.ehubportal

import grails.config.Config
import grails.core.support.GrailsConfigurationAware
import org.apache.http.client.utils.URIBuilder


class WorkflowService implements GrailsConfigurationAware {
    String apiUrl
    String apiVersion

    @Override
    void setConfiguration(Config co) {
        apiUrl = co.getProperty('api.url')
        apiVersion = co.getProperty('api.version')
    }

    def listWorkflow(String db, String offset, String limit, String sort){
        URIBuilder workflowURL = new URIBuilder(apiUrl +"/" +Enums.apiName.workflow.toString() +"/" +apiVersion +"/" +Enums.apiAction.list.toString())

        def params = APIService.buildParams(db, offset, limit, sort,null)
        workflowURL.addParameters(params)
        def url = new URL(workflowURL.build().toString())

        HttpURLConnection response = (HttpURLConnection) url.openConnection()
        response.setRequestMethod(Enums.httpMethod.GET.toString())
        // connection.setConnectTimeout(10000)
        response.connect()

        println '===List Workflow==='
        println response.toString()

        if (response.responseCode == 200 || response.responseCode == 201) {
            return response
        } else {
        }

        null
    }

    def addWorkflow(String db, Workflow workflow){

    }

    def updateWorkflow(String db, Workflow workflow, String filter){

    }

    def deleteWorkflow(String db, Workflow workflow, String filter){

    }

    def searchWorkflow(String db, int offset, int limit, String sort, String filter){

    }
}