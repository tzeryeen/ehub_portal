package com.b2b.ehubportal

import grails.config.Config
import grails.core.support.GrailsConfigurationAware
import org.apache.http.client.utils.URIBuilder

class DocumentContentMappingService implements GrailsConfigurationAware {
    String apiUrl
    String apiVersion

    @Override
    void setConfiguration(Config co) {
        apiUrl = co.getProperty('api.url')
        apiVersion = co.getProperty('api.version')
    }

    def deleteDocumentContentMapping(String db, String filter) {
        URIBuilder dcmURL = new URIBuilder(apiUrl + "/" + Enums.apiName.documentcontentmapping.toString() + "/" + apiVersion + "/" + Enums.apiAction.delete.toString())

        def params = APIService.buildParams(db, null, null, null, filter)
        dcmURL.addParameters(params)
        def url = new URL(dcmURL.build().toString())

        HttpURLConnection response = (HttpURLConnection) url.openConnection()
        response.setRequestMethod(Enums.httpMethod.GET.toString())
        // connection.setConnectTimeout(10000)
        response.connect()

        println '===Delete DCM===' + response.responseCode

        if (response.responseCode == 200 || response.responseCode == 202) {
            return response
        } else {
        }
        null
    }

    def listDocumentContentMapping(String db, String offset, String limit, String sort){
        URIBuilder dcmURL = new URIBuilder(apiUrl +"/" +Enums.apiName.documentcontentmapping.toString() +"/" +apiVersion +"/" +Enums.apiAction.list.toString())

        def params = APIService.buildParams(db, offset, limit, sort,null)
        dcmURL.addParameters(params)
        def url = new URL(dcmURL.build().toString())

        HttpURLConnection response = (HttpURLConnection) url.openConnection()
        response.setRequestMethod(Enums.httpMethod.GET.toString())
        // connection.setConnectTimeout(10000)
        response.connect()

        println '===List DCM==='
        println response.toString()

        if (response.responseCode == 200 || response.responseCode == 201) {
            return response
        } else {
        }
        null
    }

    def searchDocumentContentMapping(String db, String offset, String limit, String sort, String filter){
        URIBuilder dcmURL = new URIBuilder(apiUrl +"/" +Enums.apiName.documentcontentmapping.toString() +"/" +apiVersion +"/" +Enums.apiAction.search.toString())

        def params = APIService.buildParams(db, offset, limit, sort,filter)
        dcmURL.addParameters(params)
        def url = new URL(dcmURL.build().toString())

        HttpURLConnection response = (HttpURLConnection) url.openConnection()
        response.setRequestMethod(Enums.httpMethod.GET.toString())
        // connection.setConnectTimeout(10000)
        response.connect()

        println '===Search DCM==='
        println response.toString()

        if (response.responseCode == 200 || response.responseCode == 201) {
            return response
        } else {
        }
        null
    }
}
