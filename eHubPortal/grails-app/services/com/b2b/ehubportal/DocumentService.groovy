package com.b2b.ehubportal

import grails.config.Config
import grails.core.support.GrailsConfigurationAware
import grails.gorm.transactions.Transactional
import org.apache.http.client.utils.URIBuilder

class DocumentService implements GrailsConfigurationAware {
    String apiUrl
    String apiVersion

    @Override
    void setConfiguration(Config co) {
        apiUrl = co.getProperty('api.url')
        apiVersion = co.getProperty('api.version')
    }
	
	def searchDocument(String db, String offset, String limit, String sort, String filter) {
        URIBuilder documentURL = new URIBuilder(apiUrl + "/" + Enums.apiName.document.toString() + "/" + apiVersion + "/" + Enums.apiAction.search.toString())

        def params = APIService.buildParams(db, offset, limit, sort, filter)
        documentURL.addParameters(params)
        def url = new URL(documentURL.build().toString())

        HttpURLConnection response = (HttpURLConnection) url.openConnection()
        response.setRequestMethod(Enums.httpMethod.GET.toString())
        // connection.setConnectTimeout(10000)
        response.connect()

        println '===Search Doc==='
        println response.toString()

        if (response.responseCode == 200 || response.responseCode == 201) {
            return response
        } else {
        }

        null

    }

    def listDocument(String db, String offset, String limit, String sort){
        URIBuilder documentURL = new URIBuilder(apiUrl +"/" +Enums.apiName.document.toString() +"/" +apiVersion +"/" +Enums.apiAction.list.toString())

        def params = APIService.buildParams(db, offset, limit, sort,null)
        documentURL.addParameters(params)
        def url = new URL(documentURL.build().toString())

        HttpURLConnection response = (HttpURLConnection) url.openConnection()
        response.setRequestMethod(Enums.httpMethod.GET.toString())
        // connection.setConnectTimeout(10000)
        response.connect()

        println '===List Doc==='
        println response.toString()

        if (response.responseCode == 200 || response.responseCode == 201) {
            return response
        } else {
        }
        null
    }

    def reprocessFile(String db, String filter, String payload){
        println "===ReprocessFile==="
        println "Payload: " + payload

        URIBuilder documentUpdateURL = new URIBuilder(apiUrl +"/" +Enums.apiName.document.toString() +"/" +apiVersion +"/" +Enums.apiAction.update.toString())

        def params = APIService.buildParams(db, null,null, null,filter)
        documentUpdateURL.addParameters(params)

        def url = new URL(documentUpdateURL.build().toString())

        HttpURLConnection response = (HttpURLConnection) url.openConnection()
        response.setRequestMethod(Enums.httpMethod.PUT.toString())
        response.setDoOutput(true)
        response.setRequestProperty("Content-Type","application/json")
        response.setRequestProperty("Accept","application/json")

        OutputStreamWriter out = new OutputStreamWriter(response.getOutputStream())
        out.write(payload)
        out.close()
        response.getInputStream()


        if (response.responseCode == 200 || response.responseCode == 202) {
            return response
        } else {

        }
        null
    }
}
