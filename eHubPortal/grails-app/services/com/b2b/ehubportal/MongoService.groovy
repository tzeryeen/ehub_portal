package com.b2b.ehubportal

import wslite.soap.SOAPClient

class MongoService {

    Mongo mongo

    public def WebServiceLookup(String paramContentId, String paramCollectionName, String paramSystemName) {
        println "println '===Web Service Lookup==='" + paramContentId + "|" + paramCollectionName + "|" + paramSystemName

        def client= new SOAPClient('http://3.244.4.236:8080/MongoWebAPI/services/FileServiceImpl?wsdl')

        def response = client.send(SOAPAction:'http://3.244.4.236:8080/MongoWebAPI/services/FileServiceImpl') {
            body {
                retrieveFile('xmlns':'http://webservice.api.mongo.b2b.com') {
                    contentId(paramContentId)
                    collectionName(paramCollectionName)
                    systemName(paramSystemName)
                }
            }
        }

        byte[] decoded = response.body.decodeBase64()
        def strDecoded = new String(decoded,"UTF-8")

        println '33==============  ' + response.body

        return strDecoded
    }
}
