package com.b2b.ehubportal

import grails.config.Config
import grails.core.support.GrailsConfigurationAware
import grails.gorm.transactions.Transactional
import org.apache.http.client.utils.URIBuilder

@Transactional
class WorkflowProcessService implements GrailsConfigurationAware {

    String apiUrl
    String apiVersion

    @Override
    void setConfiguration(Config co) {
        apiUrl = co.getProperty('api.url')
        apiVersion = co.getProperty('api.version')
    }

    def listWorkflowProcess(String db, String offset, String limit, String sort){
        URIBuilder workflowProcessURL = new URIBuilder(apiUrl +"/" +Enums.apiName.workflowprocess.toString() +"/" +apiVersion +"/" +Enums.apiAction.list.toString())

        def params = APIService.buildParams(db, offset, limit, sort,null)
        workflowProcessURL.addParameters(params)
        def url = new URL(workflowProcessURL.build().toString())

        HttpURLConnection response = (HttpURLConnection) url.openConnection()
        response.setRequestMethod(Enums.httpMethod.GET.toString())
        // connection.setConnectTimeout(10000)
        response.connect()

        println '===List WP==='
        println response.toString()

        if (response.responseCode == 200 || response.responseCode == 201) {
            return response
        } else {
        }

        null
    }

    def searchWorkflowProcess(String db, String offset, String limit, String sort, String filter){
        URIBuilder workflowProcessURL = new URIBuilder(apiUrl +"/" +Enums.apiName.workflowprocess.toString() +"/" +apiVersion +"/" +Enums.apiAction.search.toString())

        def params = APIService.buildParams(db, offset, limit, sort, filter)
        workflowProcessURL.addParameters(params)
        def url = new URL(workflowProcessURL.build().toString())

        HttpURLConnection response = (HttpURLConnection) url.openConnection()
        response.setRequestMethod(Enums.httpMethod.GET.toString())
        // connection.setConnectTimeout(10000)
        response.connect()

        println '===Search WP==='
        println response.toString()

        if (response.responseCode == 200 || response.responseCode == 201) {
            return response
        } else {
        }

        null
    }
}
