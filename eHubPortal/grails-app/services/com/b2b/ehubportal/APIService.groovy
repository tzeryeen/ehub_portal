package com.b2b.ehubportal

import org.apache.http.message.BasicNameValuePair


class APIService {

    public static def buildParams (String db, String offset, String limit, String sort, String filter){
        def params = []
        println "service########## " +db +offset +limit +filter
        if (db) {
            params.add(new BasicNameValuePair(Enums.apiParam.db.toString(),db))
        }
        if (offset) {
            params.add(new BasicNameValuePair(Enums.apiParam.offset.toString(), offset))
        }
        if (limit){
            params.add(new BasicNameValuePair(Enums.apiParam.limit.toString(), limit))
        }
        if (sort) {
            params.add(new BasicNameValuePair(Enums.apiParam.sort.toString(), sort))
        }
        if (filter) {
            params.add(new BasicNameValuePair(Enums.apiParam.filter.toString(), filter))
        }
        params
    }
}
