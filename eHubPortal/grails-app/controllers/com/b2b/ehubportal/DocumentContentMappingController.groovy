package com.b2b.ehubportal

import grails.converters.JSON
import org.grails.web.json.JSONElement

class DocumentContentMappingController {

    private static final String DEFAULT_CHARSET = "UTF-8";
    DocumentContentMappingService documentContentMappingService

    def list() {
        JSONElement jsonElement;
        JSONElement jsonElement1;
        String concatStr

        def db = params.getRequest().getParameter('db')
        def offset = params.getRequest().getParameter('offset')
        def limit = params.getRequest().getParameter('max')
        def sort = params.getRequest().getParameter('sort')

        db = db == null ? Enums.apiParamDb.ehub_aeon.toString() : db
        offset = offset == null ? '0' : offset
        limit = limit == null ? '10' : limit
        sort = sort == null ? Enums.documentContentMappingSorting.DOCID.toString() : sort

        println "before########## " + db + offset + limit + sort

        def www = documentContentMappingService.listDocumentContentMapping(db, offset, limit, sort)
        jsonElement = JSON.parse(www.getInputStream(), DEFAULT_CHARSET)
        def xxx = (Result) jsonElement
        def yyy = [:]
        yyy = xxx.getResult().getData()

        String[] dcmCount = www.getHeaderField("Content-Range").split("/")
        int totalRows = Integer.parseInt(dcmCount[1])

        def params = [:]
        params.put("db", db)
        params.put("sort", sort)

        // if xxx.getResult().getData() return null value (cant get data, or only single records)
        if (yyy == null) {
            int searchIndex = jsonElement.toString().indexOf('"data":{', 0)
            if (searchIndex < 0)
                println "No data found"
            else {
                // If single records return, add square bracket for it
                String strBuild = jsonElement.toString()
                def startIndex = searchIndex + 7
                int endIndex = strBuild.indexOf('}', startIndex)
                concatStr = strBuild.substring(0, startIndex) + "[" + strBuild.substring(startIndex, endIndex + 1) + "]" + strBuild.substring(endIndex + 1, strBuild.length())

                jsonElement1 = JSON.parse(concatStr)
                def str2Json = (Result) jsonElement1

                yyy = str2Json.getResult().getData()
                DocumentContentMapping sss = new DocumentContentMapping(yyy[0])
                println " 3>>>>>>>>>>>>>>>>>>>>>>>>>>>>>  " + sss.getDocId()
                render(view: "index", model: [dcmList: yyy, dcmCount: totalRows, params: params])
            }
        } else {
            DocumentContentMapping sss = new DocumentContentMapping(yyy[0])
            println " 3>>>>>>>>>>>>>>>>>>>>>>>>>>>>>  " + sss.getDocId()
            render(view: "index", model: [dcmList: yyy, dcmCount: totalRows, params: params])
        }
    }
	
	def reprocess(String cbfield) {
        JSONElement jsonElement;
        JSONElement jsonElement1;
        String concatStr

        def db = params.getRequest().getParameter('db')
        def offset = params.getRequest().getParameter('offset')
        def limit = params.getRequest().getParameter('max')
        def sort = params.getRequest().getParameter('sort')
        //def filter1 = params.getRequest().getParameter('filter')

        db = db == null ? Enums.apiParamDb.ehub_aeon.toString() : db
        offset = offset == null ? '0' : offset
        limit = limit == null ? '10' : limit
        sort = sort == null ? Enums.documentContentMappingSorting.DOCID.toString() : sort
        def filter = 'docid::'.concat(cbfield)

        println "before########## " + db + offset + limit + sort

        def www = documentContentMappingService.searchDocumentContentMapping(db, offset, limit, sort,filter)
        jsonElement = JSON.parse(www.getInputStream(), DEFAULT_CHARSET)
        def xxx = (Result) jsonElement
        def yyy = [:]
        yyy = xxx.getResult().getData()

        String[] dcmCount = www.getHeaderField("Content-Range").split("/")

        // if xxx.getResult().getData() return null value (cant get data, or only single records)
        if (yyy == null) {
            int searchIndex = jsonElement.toString().indexOf('"data":{', 0)
            if (searchIndex < 0)
                println "No data found"
            else {
                // If single records return, add square bracket for it
                String strBuild = jsonElement.toString()
                def startIndex = searchIndex + 7
                int endIndex = strBuild.indexOf('}', startIndex)
                concatStr = strBuild.substring(0, startIndex) + "[" + strBuild.substring(startIndex, endIndex + 1) + "]" + strBuild.substring(endIndex + 1, strBuild.length())

                jsonElement1 = JSON.parse(concatStr)
                def str2Json = (Result) jsonElement1

                yyy = str2Json.getResult().getData()
                DocumentContentMapping sss = new DocumentContentMapping(yyy[0])
                println " 3>>>>>>>>>>>>>>>>>>>>>>>>>>>>>  " + sss.getDocId()
                render(view: "ContentID", model: [dcmList: yyy])
            }
        } else {
            DocumentContentMapping sss = new DocumentContentMapping(yyy[0])
            println " 3>>>>>>>>>>>>>>>>>>>>>>>>>>>>>  " + sss.getDocId()
            render(view: "ContentID", model: [dcmList: yyy])
        }
    }

    def search() {
        JSONElement jsonElement;
        JSONElement jsonElement1;
        String concatStr
        def zzz = [:]

        def db = params.getRequest().getParameter('db')
        def offset = params.getRequest().getParameter('offset')
        def limit = params.getRequest().getParameter('max')
        def sort = params.getRequest().getParameter('sort')
        def filter = params.getRequest().getParameter('filter')

        db = db == null ? Enums.apiParamDb.ehub_aeon.toString() : db
        offset = offset == null ? '0' : offset
        limit = limit == null ? '10' : limit
        sort = sort == null ? Enums.documentContentMappingSorting.DOCID.toString() : sort
        filter = filter == null ? 'docid::2201941' : filter

        println "before########## " + db + offset + limit + sort

        def www = documentContentMappingService.searchDocumentContentMapping(db, offset, limit, sort, filter)
        jsonElement = JSON.parse(www.getInputStream(), DEFAULT_CHARSET)
        def xxx = (Result) jsonElement
        def yyy = [:]
        yyy = xxx.getResult().getData()

        String[] dcmCount = www.getHeaderField("Content-Range").split("/")

        // if xxx.getResult().getData() return null value (cant get data, or only single records)
        if (yyy == null) {
            int searchIndex = jsonElement.toString().indexOf('"data":{', 0)
            if (searchIndex < 0)
                println "No data found"
            else {
                // If single records return, add square bracket for it
                String strBuild = jsonElement.toString()
                def startIndex = searchIndex + 7
                int endIndex = strBuild.indexOf('}', startIndex)
                concatStr = strBuild.substring(0, startIndex) + "[" + strBuild.substring(startIndex, endIndex + 1) + "]" + strBuild.substring(endIndex + 1, strBuild.length())

                jsonElement1 = JSON.parse(concatStr)
                println jsonElement1.toString()
                def str2Json = (Result) jsonElement1

                yyy = str2Json.getResult().getData()
                DocumentContentMapping domainDCM = new DocumentContentMapping(yyy[0])
                println " 3>>>>>>>>>>>>>>>>>>>>>>>>>>>>>  " + domainDCM.getDocId()
                render(view: "search", model: [dcmList: yyy, docId: domainDCM.getDocId()])
            }
        } else {
            DocumentContentMapping sss = new DocumentContentMapping(yyy[0])
            println " 3>>>>>>>>>>>>>>>>>>>>>>>>>>>>>  " + sss.getDocId()
            render(view: "search", model: [dcmList: yyy, docId: sss.getDocId()])
        }
    }

    def delete() {
        JSONElement jsonElement;

        def db = params.getRequest().getParameter('db')
        def filter = params.getRequest().getParameter('filter')

        db = db == null ? Enums.apiParamDb.ehub_aeon.toString() : db
        filter = 'docid::2201941' + "|version!::1"

        def www = documentContentMappingService.deleteDocumentContentMapping(db, filter)
        jsonElement = JSON.parse(www.getInputStream(), DEFAULT_CHARSET)
        def xxx = (Result) jsonElement

        println "DCM RESPONSE CODE: " + xxx.getResult().getCode()

        if (xxx.getResult().getCode() == 200) {
            println "Delete successfully"
        } else {
            println "Fail to delete"
        }
    }
	
	def delete1(String cbfield ) {
        JSONElement jsonElement;

        def db = params.getRequest().getParameter('db')
        //def filter = params.getRequest().getParameter('filter')

        db = db == null ? Enums.apiParamDb.ehub_aeon.toString() : db
        def filter = 'CONTENTID::'.concat(cbfield)
        println "contentid"+filter

        def www = documentContentMappingService.deleteDocumentContentMapping(db,filter)
        jsonElement = JSON.parse(www.getInputStream(), DEFAULT_CHARSET)
        def xxx = (Result) jsonElement

        println "DCM RESPONSE CODE: "+ xxx.getResult().getCode()

        if (xxx.getResult().getCode()==200){
            println "Delete successfully"
        }
        else {
            println "Fail to delete"
        }
    }

	/*
    def getSearchContentId(String db, String result) {
        JSONElement jsonElement
        JSONElement jsonElement1
        String concatStr

        def offset = params.getRequest().getParameter('offset')
        def limit = params.getRequest().getParameter('max')
        def sort = params.getRequest().getParameter('sort')
        def filter = "docId::" + result.toString()

        db = db == null ? Enums.apiParamDb.ehub_aeon.toString() : db
        offset = offset == null ? '0' : offset
        limit = limit == null ? '10' : limit
        sort = sort == null ? Enums.documentContentMappingSorting.VERSION.toString() : sort
        println params.doNotRedirect;
        println "dcm ::::" + documentContentMappingService.listDocumentContentMapping(db, offset, limit, sort)
        def dcm = documentContentMappingService.searchDocumentContentMapping(db, offset, limit, sort, filter)

        jsonElement = JSON.parse(dcm.getInputStream(), DEFAULT_CHARSET)
        def xxx = (Result) jsonElement
        def yyy = [:]
        yyy = xxx.getResult().getData()

        // if xxx.getResult().getData() return null value (cant get data, or only single records)
        if (yyy == null) {
            int searchIndex = jsonElement.toString().indexOf('"data":{', 0)
            if (searchIndex < 0)
                println "No data found"
            else {
                // If single records return, add square bracket for it
                String strBuild = jsonElement.toString()
                def startIndex = searchIndex + 7
                int endIndex = strBuild.indexOf('}', startIndex)
                concatStr = strBuild.substring(0, startIndex) + "[" + strBuild.substring(startIndex, endIndex + 1) + "]" + strBuild.substring(endIndex + 1, strBuild.length())

                jsonElement1 = JSON.parse(concatStr)
                def str2Json = (Result) jsonElement1

                yyy = str2Json.getResult().getData()
                println "JSON ::: "+ yyy

                DocumentContentMapping docContentMapping = new DocumentContentMapping(yyy[0])
                println "hehhehehehe " + docContentMapping.getId()
                println "hehhehehehe3 " + docContentMapping.getContentTypeId()
                println "hehhehehehe1 " + docContentMapping.getComm_sequence()
                println "hehhehehehe2 " + docContentMapping.getDocumentTypeId()
                println "hehhehehehe2 " + docContentMapping.getContentId()
                println "hehhehehehe2 " + docContentMapping.getDocId()

                if(!'true'.equals(params.doNotRedirect)){
                    flash.message = "BELLO123"
                    redirect(view: "index", model: [dcmList: yyy])
                }
                return docContentMapping.getDocId() + "_" + docContentMapping.getContentId() + "_" + docContentMapping.getVersion()
            }
        } else {
            if(!'true'.equals(params.doNotRedirect)){
                flash.message = "BELLO"
                redirect(view: "index", model: [dcmList: yyy])
            }
            DocumentContentMapping docContentMapping = new DocumentContentMapping(yyy[0])
            println "hahhahahha " + docContentMapping.getVersion()
            return docContentMapping.getDocId() + "_" + docContentMapping.getContentId() + "_" + docContentMapping.getVersion()
        }
    }*/
}
