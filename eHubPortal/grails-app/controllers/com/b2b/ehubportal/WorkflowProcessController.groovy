package com.b2b.ehubportal

import grails.converters.JSON
import org.grails.web.json.JSONElement

class WorkflowProcessController {

    private static final String DEFAULT_CHARSET = "UTF-8";
    WorkflowProcessService workflowProcessService

    def list() {
        JSONElement jsonElement;
        JSONElement jsonElement1;
        String concatStr

        def db = params.getRequest().getParameter('db')
        def offset = params.getRequest().getParameter('offset')
        def limit = params.getRequest().getParameter('max')
        def sort = params.getRequest().getParameter('sort')

        db = db == null ? Enums.apiParamDb.ehub_aeon.toString() : db
        offset = offset == null ? '0' : offset
        limit = limit == null ? '10' : limit
        sort = sort == null ? Enums.workflowProcessSorting.WORKFLOWID.toString() : sort

        println "before########## " + db + offset + limit + sort

        def www = workflowProcessService.listWorkflowProcess(db, offset, limit, sort)
        jsonElement = JSON.parse(www.getInputStream(), DEFAULT_CHARSET)
        def xxx = (Result) jsonElement

        def yyy = [:]
        yyy = xxx.getResult().getData()

        String[] workflowProcessCount = www.getHeaderField("Content-Range").split("/")
        String[] range = workflowProcessCount[0].split("-")
        int totalRows = Integer.parseInt(workflowProcessCount[1])

        def params = [:]
        params.put("db", db)
        params.put("sort", sort)

        // if xxx.getResult().getData() return null value (cant get data, or only single records)
        if (yyy == null) {
            int searchIndex = jsonElement.toString().indexOf('"data":{', 0)
            if (searchIndex < 0)
                render "No data found"
            else {
                // If single records return, add square bracket for it
                String strBuild = jsonElement.toString()
                def startIndex = searchIndex + 7
                int endIndex = strBuild.indexOf('}', startIndex)
                concatStr = strBuild.substring(0, startIndex) + "[" + strBuild.substring(startIndex, endIndex + 1) + "]" + strBuild.substring(endIndex + 1, strBuild.length())

                jsonElement1 = JSON.parse(concatStr)
                def str2Json = (Result) jsonElement1

                yyy = str2Json.getResult().getData()
                WorkflowProcess sss = new WorkflowProcess(yyy[0])
                println " 3>>>>>>>>>>>>>>>>>>>>>>>>>>>>>  " + sss.getSequenceOrder()
                render(view: "index", model: [workflowProcessList: yyy, workflowProcessCount: totalRows, params: params])
            }
        } else {
            WorkflowProcess sss = new WorkflowProcess(yyy[0])
            println " 3>>>>>>>>>>>>>>>>>>>>>>>>>>>>>  " + sss.getSequenceOrder()
            render(view: "index", model: [workflowProcessList: yyy, workflowProcessCount: totalRows, params: params])
        }
    }

    def search() {
        JSONElement jsonElement;
        JSONElement jsonElement1;
        String concatStr

        def db = params.getRequest().getParameter('db')
        def offset = params.getRequest().getParameter('offset')
        def limit = params.getRequest().getParameter('max')
        def sort = params.getRequest().getParameter('sort')
        def filter = params.getRequest().getParameter('filter')

        db = db == null ? Enums.apiParamDb.ehub_aeon.toString() : db
        offset = offset == null ? '0' : offset
        limit = limit == null ? '10' : limit
        sort = sort == null ? Enums.workflowProcessSorting.SEQUENCEORDER.toString() : sort
        filter = filter == null ? '' : filter

        println "before########## " + db + offset + limit + sort

        def www = workflowProcessService.searchWorkflowProcess(db, offset, limit, sort, filter)
        jsonElement = JSON.parse(www.getInputStream(), DEFAULT_CHARSET)
        def xxx = (Result) jsonElement
        def yyy = [:]
        yyy = xxx.getResult().getData()

        String[] workflowProcessCount = www.getHeaderField("Content-Range").split("/")
        String[] range = workflowProcessCount[0].split("-")
        int totalRows = Integer.parseInt(workflowProcessCount[1])

        def params = [:]
        params.put("db", db)
        params.put("sort", sort)
        params.put("filter", filter)

        // if xxx.getResult().getData() return null value (cant get data, or only single records)
        if (yyy == null) {
            int searchIndex = jsonElement.toString().indexOf('"data":{', 0)
            if (searchIndex < 0) {
                render "No data found"
            }else {
                // If single records return, add square bracket for it
                String strBuild = jsonElement.toString()
                def startIndex = searchIndex + 7
                int endIndex = strBuild.indexOf('}', startIndex)
                concatStr = strBuild.substring(0, startIndex) + "[" + strBuild.substring(startIndex, endIndex + 1) + "]" + strBuild.substring(endIndex + 1, strBuild.length())

                jsonElement1 = JSON.parse(concatStr)
                def str2Json = (Result) jsonElement1

                yyy = str2Json.getResult().getData()
                WorkflowProcess sss = new WorkflowProcess(yyy[0])
                println " 3>>>>>>>>>>>>>>>>>>>>>>>>>>>>>  " + sss.getWorkflowID()
                println 'totalRows123?' + totalRows
                render(view: "search", model: [workflowProcessList: yyy, workflowProcessCount: totalRows, workflowId: sss.getWorkflowID(), params: params])
            }
        } else {
            WorkflowProcess sss = new WorkflowProcess(yyy[0])
            println " 3>>>>>>>>>>>>>>>>>>>>>>>>>>>>>  " + sss.getWorkflowID()
            println 'totalRows?' + totalRows
            render(view: "search", model: [workflowProcessList: yyy, workflowProcessCount: totalRows, workflowId: sss.getWorkflowID(), params: params])
        }

    }
}
