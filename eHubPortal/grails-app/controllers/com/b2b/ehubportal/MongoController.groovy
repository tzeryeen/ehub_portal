package com.b2b.ehubportal

class MongoController {

    private static final String DEFAULT_CHARSET = "UTF-8"
    MongoService mongoService

    //Need to pass in the contentid, collectionName, systemName
    def view() {
        def contentId = params.getRequest().getParameter('contentId')
        def collectionName = params.getRequest().getParameter('collectionName')
        def systemName = params.getRequest().getParameter('systemName')

        contentId = contentId == null ? '9786132' : contentId
        collectionName = collectionName == null ? 'DOCUMENT_CONTENT' : collectionName
        systemName = systemName == null ? 'eHub_mssql_QA' : systemName

        def test = mongoService.WebServiceLookup(contentId,collectionName,systemName)
        println "testing ##########"

        render(view: "view", model:[xmlString:test])
    }

    def download() {
        def contentId = params.getRequest().getParameter('contentId')
        def collectionName = params.getRequest().getParameter('DOCUMENT_CONTENT')
        def systemName = params.getRequest().getParameter('eHub_mssql_QA')

        contentId = contentId == null ? '9786132' : contentId
        collectionName = collectionName == null ? 'DOCUMENT_CONTENT' : collectionName
        systemName = systemName == null ? 'eHub_mssql_QA' : systemName

        def test = mongoService.WebServiceLookup(contentId,collectionName,systemName)
        if (test == null) {
            flash.message = "Document not found."
            redirect(action: 'view')
        } else {
            response.setContentType("APPLICATION/OCTET-STREAM")
            response.setHeader("Content-Disposition", "Attachment;Filename=\"${contentId}\"")
            def outputStream = response.getOutputStream()
            outputStream << test
            outputStream.flush()
            outputStream.close()
        }
    }
}
