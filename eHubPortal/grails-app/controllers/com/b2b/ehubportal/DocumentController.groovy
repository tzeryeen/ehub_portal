package com.b2b.ehubportal

import grails.converters.JSON
import org.grails.web.json.JSONElement

class DocumentController {

    private static final String DEFAULT_CHARSET = "UTF-8";
    DocumentService documentService
    DocumentContentMappingService documentContentMappingService
    DocumentContentService documentContentService
    def checklist;

    def list() {
        JSONElement jsonElement;
        JSONElement jsonElement1;
        String concatStr
        def db = params.getRequest().getParameter('db')
        def offset = params.getRequest().getParameter('offset')
        def limit = params.getRequest().getParameter('max')
        def sort = params.getRequest().getParameter('sort')

        db = db == null ? Enums.apiParamDb.ehub_aeon.toString() : db
        offset = offset == null ? '0' : offset
        limit = limit == null ? '10' : limit
        sort = sort == null ? Enums.documentSorting.SENDERMAILBOXID.toString() : sort
        println "before########## " + db + offset + limit + sort
        def www = documentService.listDocument(db, offset, limit, sort)

        jsonElement = JSON.parse(www.getInputStream(), DEFAULT_CHARSET)
        def xxx = (Result) jsonElement
        def yyy = [:]
        yyy = xxx.getResult().getData()

        String[] documentCount = www.getHeaderField("Content-Range").split("/")
        String[] range = documentCount[0].split("-")
        int totalRows = Integer.parseInt(documentCount[1])

        def params = [:]
        params.put("db", db)
        params.put("sort", sort)

        // if xxx.getResult().getData() return null value (cant get data, or only single records)
        if (yyy == null) {
            int searchIndex = jsonElement.toString().indexOf('"data":{', 0)
            if (searchIndex < 0)
                println "No data found"
            else {
                // If single records return, add square bracket for it
                String strBuild = jsonElement.toString()
                def startIndex = searchIndex + 7
                int endIndex = strBuild.indexOf('}', startIndex)
                concatStr = strBuild.substring(0, startIndex) + "[" + strBuild.substring(startIndex, endIndex + 1) + "]" + strBuild.substring(endIndex + 1, strBuild.length())

                jsonElement1 = JSON.parse(concatStr)
                def str2Json = (Result) jsonElement1

                yyy = str2Json.getResult().getData()
                Document sss = new Document(yyy[0])
                println " 3>>>>>>>>>>>>>>>>>>>>>>>>>>>>>  " + sss.getDocId()
                render(view: "list", model: [documentList: yyy, documentCount: totalRows, params: params])
            }
        } else {
            Document sss = new Document(yyy[0])
            println " 3>>>>>>>>>>>>>>>>>>>>>>>>>>>>>  " + sss.getDocId()
            render(view: "list", model: [documentList: yyy, documentCount: totalRows, params: params])

        }
    }

    def index() {
        render(view: "index")
    }

    def update() {
        render(view: "update")
    }

    def submitForm(String cbfield) {
        render "The value of cbfield is: ${cbfield}"
    }

    def metadata() {
        JSONElement jsonElement;
        JSONElement jsonElement1;
        String concatStr

        def db = params.getRequest().getParameter('db')
        def offset = params.getRequest().getParameter('offset')
        def limit = params.getRequest().getParameter('max')
        def sort = params.getRequest().getParameter('sort')

        def docID = params.getRequest().getParameter('categoryName')
        String docIdTextBox = "DOCID" + "::"
        def filter = docIdTextBox.concat(docID)
        db = db == null ? Enums.apiParamDb.ehub_aeon.toString() : db
        offset = offset == null ? '0' : offset
        limit = limit == null ? '10' : limit
        sort = sort == null ? Enums.documentSorting.SENDERMAILBOXID.toString() : sort

        println "before##########" + db + offset + limit + sort + filter
        def www = documentService.searchDocument(db, offset, limit, sort, filter)
        jsonElement = JSON.parse(www.getInputStream(), DEFAULT_CHARSET)
        def xxx = (Result) jsonElement
        def yyy = [:]
        yyy = xxx.getResult().getData()

        String[] documentCount = www.getHeaderField("Content-Range").split("/")
        String[] range = documentCount[0].split("-")
        int totalRows = Integer.parseInt(documentCount[1])

        def params = [:]
        params.put("db", db)
        params.put("sort", sort)

        // if xxx.getResult().getData() return null value (cant get data, or only single records)
        if (yyy == null) {
            int searchIndex = jsonElement.toString().indexOf('"data":{', 0)
            if (searchIndex < 0)
                println "No data found"
            else {
                // If single records return, add square bracket for it
                String strBuild = jsonElement.toString()
                def startIndex = searchIndex + 7
                int endIndex = strBuild.indexOf('}', startIndex)
                concatStr = strBuild.substring(0, startIndex) + "[" + strBuild.substring(startIndex, endIndex + 1) + "]" + strBuild.substring(endIndex + 1, strBuild.length())

                jsonElement1 = JSON.parse(concatStr)
                def str2Json = (Result) jsonElement1

                yyy = str2Json.getResult().getData()
                Document sss = new Document(yyy[0])
                println " 3>>>>>>>>>>>>>>>>>>>>>>>>>>>>>  " + sss.getDocId()
                render(view: "metadata", model: [documentSearch: yyy])
            }
        } else {
            Document sss = new Document(yyy[0])
            println " 3>>>>>>>>>>>>>>>>>>>>>>>>>>>>>  " + sss.getDocId()
            render(view: "metadata", model: [documentSearch: yyy])

        }
    }

    def searchDoc() {
        JSONElement jsonElement;
        JSONElement jsonElement1;
        String concatStr

        def db = params.getRequest().getParameter('db')
        def offset = params.getRequest().getParameter('offset')
        def limit = params.getRequest().getParameter('max')
        def sort = params.getRequest().getParameter('sort')
        def docID = params.getRequest().getParameter('docID')
        def senderMailboxId = params.getRequest().getParameter('senderMailboxId')
        def receiverMailboxId = params.getRequest().getParameter('receiverMailboxId')
        def docTypeId = params.getRequest().getParameter('docTypeId')
        def docFileName = params.getRequest().getParameter('docFileName')
        def status = params.getRequest().getParameter('status')
        def creationFromDate=params.getRequest().getParameter('creationFromDate')
        def creationToDate=params.getRequest().getParameter('creationToDate')
        println "FROMDATE"+creationFromDate
        println "TODATE"+creationToDate
        String harcode=" CREATIONDATE <::"

        def concatFromDateToDate="CREATIONDATE >::".concat(creationFromDate).concat("|").concat(harcode).concat(creationToDate)

        String SenderMailboxIdTextBox = "SENDERMAILBOXID" + "::"
        String ReceiverMailboxIdTextBox = "RECEIVERMAILBOXID" + "::"
        String DocTypeIdTextBox = "DOCTYPEID" + "::"
        String docIdTextBox = "DOCID" + "::"
        String docFileNameTextBox = "DOCFILENAME" + "::"
        String statusTextBox = "STATUS" + "::"


        def concatSenderMailboxId = SenderMailboxIdTextBox.concat(senderMailboxId)
        def concatReceiverMailboxId = ReceiverMailboxIdTextBox.concat(receiverMailboxId)
        def concatDocTypeId = DocTypeIdTextBox.concat(docTypeId)
        def concatDocid = docIdTextBox.concat(docID)
        def concatDocFileName = docFileNameTextBox.concat(docFileName)
        def concatStatus = statusTextBox.concat(status)
        def filter = concatSenderMailboxId.concat("|").concat(concatReceiverMailboxId).concat("|").concat(concatDocTypeId).concat("|").concat(concatDocid).concat("|").concat(concatDocFileName).concat("|").concat(concatStatus).concat("|").concat(concatFromDateToDate)

        db = db == null ? Enums.apiParamDb.ehub_aeon.toString() : db
        offset = offset == null ? '0' : offset
        limit = limit == null ? '10' : limit
        sort = sort == null ? Enums.documentSorting.SENDERMAILBOXID.toString() : sort

        println "before##########" + db + offset + limit + sort + filter
        def www = documentService.searchDocument(db, offset, limit, sort, filter)
        println "Test" + www
        jsonElement = JSON.parse(www.getInputStream(), DEFAULT_CHARSET)
        def xxx = (Result) jsonElement
        def yyy = [:]
        yyy = xxx.getResult().getData()

        String[] documentCount = www.getHeaderField("Content-Range").split("/")
        String[] range = documentCount[0].split("-")
        int totalRows = Integer.parseInt(documentCount[1])

        def params = [:]
        params.put("db", db)
        params.put("sort", sort)
        params.put("status", status)
        params.put("docID", docID)
        params.put("senderMailboxId", senderMailboxId)
        params.put("receiverMailboxId", receiverMailboxId)
        params.put("docTypeId", docTypeId)
        params.put("docFileName", docFileName)
        params.put("creationFromDate",creationFromDate)
        params.put("creationToDate",creationToDate)

        // if xxx.getResult().getData() return null value (cant get data, or only single records)
        if (yyy == null) {
            int searchIndex = jsonElement.toString().indexOf('"data":{', 0)
            if (searchIndex < 0)
                println "No data found"
            else {
                // If single records return, add square bracket for it
                String strBuild = jsonElement.toString()
                def startIndex = searchIndex + 7
                int endIndex = strBuild.indexOf('}', startIndex)
                concatStr = strBuild.substring(0, startIndex) + "[" + strBuild.substring(startIndex, endIndex + 1) + "]" + strBuild.substring(endIndex + 1, strBuild.length())

                jsonElement1 = JSON.parse(concatStr)
                def str2Json = (Result) jsonElement1

                yyy = str2Json.getResult().getData()
                Document sss = new Document(yyy[0])
                println " 3>>>>>>>>>>>>>>>>>>>>>>>>>>>>>  " + sss.getDocId()
                render(view: "search", model: [documentSearch: yyy, documentCount: totalRows, params: params])
            }
        } else {
            Document sss = new Document(yyy[0])
            println " 3>>>>>>>>>>>>>>>>>>>>>>>>>>>>>  " + sss.getDocId()
            render(view: "search", model: [documentSearch: yyy, documentCount: totalRows, params: params])
        }
    }

    def displayUpdate() {
        JSONElement jsonElement;
        JSONElement jsonElement1;
        String concatStr

        def db = params.getRequest().getParameter('db')
        def offset = params.getRequest().getParameter('offset')
        def limit = params.getRequest().getParameter('max')
        def sort = params.getRequest().getParameter('sort')
        def docID = params.getRequest().getParameter('docID')
        def senderMailboxId = params.getRequest().getParameter('senderMailboxId')
        def receiverMailboxId = params.getRequest().getParameter('receiverMailboxId')
        def docTypeId = params.getRequest().getParameter('docTypeId')
        def docFileName = params.getRequest().getParameter('docFileName')
        def status = params.getRequest().getParameter('status')
        def creationFromDate=params.getRequest().getParameter('creationFromDate')
        def creationToDate=params.getRequest().getParameter('creationToDate')
        println "FROMDATE"+creationFromDate
        println "TODATE"+creationToDate
        String harcode=" CREATIONDATE <::"
        def concatFromDateToDate="CREATIONDATE >::".concat(creationFromDate).concat("|").concat(harcode).concat(creationToDate)


        String SenderMailboxIdTextBox = "SENDERMAILBOXID" + "::"
        String ReceiverMailboxIdTextBox = "RECEIVERMAILBOXID" + "::"
        String DocTypeIdTextBox = "DOCTYPEID" + "::"
        String docIdTextBox = "DOCID" + "::"
        String docFileNameTextBox = "DOCFILENAME" + "::"
        String statusTextBox = "STATUS" + "::"

        def concatSenderMailboxId = SenderMailboxIdTextBox.concat(senderMailboxId)
        def concatReceiverMailboxId = ReceiverMailboxIdTextBox.concat(receiverMailboxId)
        def concatDocTypeId = DocTypeIdTextBox.concat(docTypeId)
        def concatDocid = docIdTextBox.concat(docID)
        def concatDocFileName = docFileNameTextBox.concat(docFileName)
        def concatStatus = statusTextBox.concat(status)

        def filter = concatSenderMailboxId.concat("|").concat(concatReceiverMailboxId).concat("|").concat(concatDocTypeId).concat("|").concat(concatDocid).concat("|").concat(concatDocFileName).concat("|").concat(concatStatus).concat("|").concat(concatFromDateToDate)
        db = db == null ? Enums.apiParamDb.ehub_general.toString() : db
        offset = offset == null ? '0' : offset
        limit = limit == null ? '10' : limit
        sort = sort == null ? Enums.documentSorting.SENDERMAILBOXID.toString() : sort

        println "before##########" + db + offset + limit + sort + filter
        def www = documentService.searchDocument(db, offset, limit, sort, filter)
        println "Test" + www
        jsonElement = JSON.parse(www.getInputStream(), DEFAULT_CHARSET)
        def xxx = (Result) jsonElement
        def yyy = [:]
        yyy = xxx.getResult().getData()

        String[] documentCount = www.getHeaderField("Content-Range").split("/")
        println "DocumentCount" + documentCount
        String[] range = documentCount[0].split("-")
        println "range" + range
        int totalRows = Integer.parseInt(documentCount[1])
        println "totalRows" + totalRows

        def params = [:]
        params.put("db", db)
        params.put("sort", sort)
        params.put("status", status)
        params.put("docID", docID)
        params.put("senderMailboxId", senderMailboxId)
        params.put("receiverMailboxId", receiverMailboxId)
        params.put("docTypeId", docTypeId)
        params.put("docFileName", docFileName)
        params.put("creationFromDate",creationFromDate)
        params.put("creationToDate",creationToDate)

        // if xxx.getResult().getData() return null value (cant get data, or only single records)
        if (yyy == null) {
            int searchIndex = jsonElement.toString().indexOf('"data":{', 0)
            if (searchIndex < 0)
                println "No data found"
            else {
                // If single records return, add square bracket for it
                String strBuild = jsonElement.toString()
                def startIndex = searchIndex + 7
                int endIndex = strBuild.indexOf('}', startIndex)
                concatStr = strBuild.substring(0, startIndex) + "[" + strBuild.substring(startIndex, endIndex + 1) + "]" + strBuild.substring(endIndex + 1, strBuild.length())

                jsonElement1 = JSON.parse(concatStr)
                def str2Json = (Result) jsonElement1

                yyy = str2Json.getResult().getData()
                Document sss = new Document(yyy[0])
                println " 3>>>>>>>>>>>>>>>>>>>>>>>>>>>>>  " + sss.getDocId()
                render(view: "displayUpdate", model: [documentSearch: yyy, documentCount: totalRows, params: params])
            }
        } else {
            Document sss = new Document(yyy[0])
            println " 3>>>>>>>>>>>>>>>>>>>>>>>>>>>>>  " + sss.getDocId()
            render(view: "displayUpdate", model: [documentSearch: yyy, documentCount: totalRows, params: params])
        }
    }

    def getSearchContentId(String db, String result) {
        JSONElement jsonElement
        JSONElement jsonElement1
        String concatStr

        def offset = params.getRequest().getParameter('offset')
        def limit = params.getRequest().getParameter('max')
        def sort = params.getRequest().getParameter('sort')
        def filter = "docId::" + result.toString()

        db = db == null ? Enums.apiParamDb.ehub_aeon.toString() : db
        offset = offset == null ? '0' : offset
        limit = limit == null ? '10' : limit
        sort = sort == null ? Enums.documentContentMappingSorting.VERSION.toString() : sort

        println "dcm ::::" + documentContentMappingService.listDocumentContentMapping(db, offset, limit, sort)
        def dcm = documentContentMappingService.searchDocumentContentMapping(db, offset, limit, sort, filter)

        jsonElement = JSON.parse(dcm.getInputStream(), DEFAULT_CHARSET)
        def xxx = (Result) jsonElement
        def yyy = [:]
        yyy = xxx.getResult().getData()

        // if xxx.getResult().getData() return null value (cant get data, or only single records)
        if (yyy == null) {
            int searchIndex = jsonElement.toString().indexOf('"data":{', 0)
            if (searchIndex < 0)
                println "No data found"
            else {
                // If single records return, add square bracket for it
                String strBuild = jsonElement.toString()
                def startIndex = searchIndex + 7
                int endIndex = strBuild.indexOf('}', startIndex)
                concatStr = strBuild.substring(0, startIndex) + "[" + strBuild.substring(startIndex, endIndex + 1) + "]" + strBuild.substring(endIndex + 1, strBuild.length())

                jsonElement1 = JSON.parse(concatStr)
                def str2Json = (Result) jsonElement1

                yyy = str2Json.getResult().getData()

                yyy.each { contentId ->
                    DocumentContentMapping docContentMapping = new DocumentContentMapping(contentId)
                    return docContentMapping.getDocId() + "_" + docContentMapping.getContentId() + "_" + docContentMapping.getVersion1()
                };
            }
        } else {
            yyy.each { contentId ->
                DocumentContentMapping docContentMapping = new DocumentContentMapping(contentId)
                return docContentMapping.getDocId() + "_" + docContentMapping.getContentId() + "_" + docContentMapping.getVersion1()
            };
        }
    }

    def reprocess() {
        def status = 85
        def state = 'NOOP'
        def lastSuccessfulState = 'NOOP'
        def previousState = 'SUBMIT'
        def latestSequenceOrder = 2
        def retryCountShort = 5
        def retryCountLong = 3
        def lastErrorDateTime = null
        def lastErrorTypeId = 0
        def version = 1
        def chkboxDocId
        int docId, strContentId, strVersion
        def dcmResponseCode, dcResponseCode
        boolean checked = false;
        // def metadata = "metadata.modify('delete (/METADATA[1]/validate ,/METADATA[1]/validateHeader, /METADATA[1]/errDesc)')"

        checklist = params.list('checklist')
        def db = params.getRequest().getParameter('db')

        db = db == null ? Enums.apiParamDb.ehub_aeon.toString() : db
        println "===REPROCESSING==="
        println "IM checklist size: " + checklist.size()

        if (checklist.size() != 0) {
            for (result in checklist) {
                chkboxDocId = result.toString()

                def paramContent = [:]
                paramContent = getSearchContentId(db, chkboxDocId)
                println "paramContent: " + paramContent

                paramContent.each { contentId ->
                    DocumentContentMapping docContentMapping = new DocumentContentMapping(contentId)

                    docId = docContentMapping.getDocId()
                    strContentId = docContentMapping.getContentId()
                    strVersion = docContentMapping.getVersion1()

                    String filter = "docId::" + docId
                    String filterDC = "contentid::" + strContentId
                    String filterDCM = "contentid::" + strContentId + "|version!::1"

                    println "FILTER: " + filter + ", DC: " + filterDC + ", DCM: " + filterDCM

                    //concat every param pass in with "FIELDNAME": "VALUE"
                    String payload = "{\"STATUS\":\"" + status.toString() + "\",\"STATE\":\"" + state +
                            "\",\"LAST_SUCCESSFUL_STATE\":\"" + lastSuccessfulState + "\",\"PREVIOUS_STATE\":\"" + previousState +
                            "\",\"LASTEST_SEQUENCEORDER\":\"" + latestSequenceOrder + "\",\"RETRYCOUNT_SHORT\":\"" + retryCountShort +
                            "\",\"RETRYCOUNT_LONG\":\"" + retryCountLong + "\",\"LASTERRORDATETIME\":\"" + lastErrorDateTime +
                            "\",\"LASTERRORTYPEID\":\"" + lastErrorTypeId + "\",\"VERSION\":" + version + "}"

                    if (strVersion != 1) {
                        println "version::" + strVersion

                        dcmResponseCode = documentContentMappingService.deleteDocumentContentMapping(db, filterDCM).responseCode
                        dcResponseCode = documentContentService.deleteDocumentContent(db, filterDC).responseCode

                        if (dcmResponseCode == 200 || dcmResponseCode == 202) {
                            if (dcResponseCode == 200 || dcResponseCode == 202) {
                                //if response return from dss is ok, then display updated successfully message
                                int statusResponse = documentService.reprocessFile(db, filter, payload).responseCode

                                if (statusResponse == 200 || statusResponse == 202) {
                                    println "deleted dcm and dc. update successfully"
                                    checked = true
                                } else {
                                    println "fail to update."
                                }
                            } else{
                                println "fail to delete dc."
                            }
                        } else {
                            println "fail to delete dcm"
                        }
                    } else {
                        //if response return from dss is ok, then display updated successfully message
                        int statusResponse = documentService.reprocessFile(db, filter, payload).responseCode

                        if (statusResponse == 200 || statusResponse == 202) {
                            println "update successfully"
                            checked = true
                        } else {
                            println "fail to update."
                        }
                    }
                }
            }
            if (checked == true){
                println "IM checklist: " + checklist
                reprocessBatch()
            }else{
                render "fail to reprocess."
            }
        }
        else{
            def message = "no checkbox have ticked."
            render message
        }
    }

    def reprocessBatch(){
        JSONElement jsonElement;
        JSONElement jsonElement1;
        String concatStr

        def db = params.getRequest().getParameter('db')
        def offset = params.getRequest().getParameter('offset')
        def limit = params.getRequest().getParameter('max')
        def sort = params.getRequest().getParameter('sort')
        def filter=''

        if(checklist.size() > 1){
            def lastElement = checklist[checklist.size()-1]
            for (result in checklist) {
                if(result == lastElement)
                    filter += "docid::" + result.toString()
                else{
                    filter += "docid::" + result.toString() + "' OR "
                }
            }
        }else{
            filter += "docid::" + checklist[0]
        }

        println "IM filter at reprocessBatch: " + filter
        db = db == null ? Enums.apiParamDb.ehub_general.toString() : db
        offset = offset == null ? '0' : offset
        limit = limit == null ? '10' : limit
        sort = sort == null ? Enums.documentSorting.SENDERMAILBOXID.toString() : sort

        println "before########## " + db + offset + limit + sort + filter
        def www = documentService.searchDocument(db, offset, limit, sort, filter)

        jsonElement = JSON.parse(www.getInputStream(), DEFAULT_CHARSET)
        def xxx = (Result) jsonElement
        def yyy = [:]
        yyy = xxx.getResult().getData()

        String[] documentCount = www.getHeaderField("Content-Range").split("/")
        String[] range = documentCount[0].split("-")
        int totalRows = Integer.parseInt(documentCount[1])

        def params = [:]
        params.put("db", db)
        params.put("sort", sort)
        params.put("filter", filter)

        // if xxx.getResult().getData() return null value (cant get data, or only single records)
        if (yyy == null) {
            int searchIndex = jsonElement.toString().indexOf('"data":{', 0)
            if (searchIndex < 0)
                println "No data found"
            else {
                // If single records return, add square bracket for it
                String strBuild = jsonElement.toString()
                def startIndex = searchIndex + 7
                int endIndex = strBuild.indexOf('}', startIndex)
                concatStr = strBuild.substring(0, startIndex) + "[" + strBuild.substring(startIndex, endIndex + 1) + "]" + strBuild.substring(endIndex + 1, strBuild.length())

                jsonElement1 = JSON.parse(concatStr)
                def str2Json = (Result) jsonElement1

                yyy = str2Json.getResult().getData()
                Document sss = new Document(yyy[0])

                render(view: "reprocessBatch", model: [documentSearch: yyy, documentCount: totalRows, params: params])
            }
        } else {
            Document sss = new Document(yyy[0])

            render(view: "reprocessBatch", model: [documentSearch: yyy, documentCount: totalRows, params: params])
        }
    }

}
