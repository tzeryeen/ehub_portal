package com.b2b.ehubportal

class UrlMappings {

    static mappings = {
        "/$controller/$action?/$id?(.$format)?"{
            constraints {
                // apply constraints here
            }
        }

        "/workflow"(controller: "workflow", action: "list")
        "/workflowprocess"(controller: "workflowprocess", action: "list")
        "/workflowprocess"(controller: "workflowprocess", action: "search")
        "/document"(controller: "document", action: "search")
        "/document"(controller: "document", action: "list")
        "/document"(controller: "document", action: "update")
        "/documentcontentmapping"(controller: "documentcontentmapping", action: "list")
        "/documentcontentmapping"(controller: "documentcontentmapping", action: "search")
		"/home"(view:"/home")
        "/"(view:"/index")
        "500"(view:'/error')
        "404"(view:'/notFound')
    }
}
