package com.b2b.ehubportal

import grails.converters.JSON
import org.apache.commons.lang.ObjectUtils
import org.grails.web.json.JSONElement

class WorkflowController {

    private static final String DEFAULT_CHARSET = "UTF-8";
    WorkflowService workflowService

    def list() {
        JSONElement jsonElement;
        JSONElement jsonElement1;
        String concatStr

        def db = params.getRequest().getParameter('db')
        def offset = params.getRequest().getParameter('offset')
        def limit = params.getRequest().getParameter('max')
        def sort = params.getRequest().getParameter('sort')

        db = db == null ? Enums.apiParamDb.ehub_aeon.toString() : db
        offset = offset == null ? '0' : offset
        limit = limit == null ? '10' : limit
        sort = sort == null ? Enums.workflowSorting.WORKFLOWID.toString() : sort

        println "before########## " + db + offset + limit + sort

        def www = workflowService.listWorkflow(db, offset, limit, sort)
        jsonElement = JSON.parse(www.getInputStream(), DEFAULT_CHARSET)
        def xxx = (Result) jsonElement
        def yyy = [:]
        yyy = xxx.getResult().getData()

        String[] workflowCount = www.getHeaderField("Content-Range").split("/")
        String[] range = workflowCount[0].split("-")
        int totalRows = Integer.parseInt(workflowCount[1])

        def params = [:]
        params.put("db", db)
        params.put("sort", sort)

        // if xxx.getResult().getData() return null value (cant get data, or only single records)
        if (yyy == null) {
            int searchIndex = jsonElement.toString().indexOf('"data":{', 0)
            if (searchIndex < 0)
                render "No data found"
            else {
                // If single records return, add square bracket for it
                String strBuild = jsonElement.toString()
                def startIndex = searchIndex + 7
                int endIndex = strBuild.indexOf('}', startIndex)
                concatStr = strBuild.substring(0, startIndex) + "[" + strBuild.substring(startIndex, endIndex + 1) + "]" + strBuild.substring(endIndex + 1, strBuild.length())

                jsonElement1 = JSON.parse(concatStr)
                def str2Json = (Result) jsonElement1

                yyy = str2Json.getResult().getData()
                Workflow sss = new Workflow(yyy[0])
                println " 3>>>>>>>>>>>>>>>>>>>>>>>>>>>>>  " + sss.getWorkflowId()
                render(view: "index", model: [workflowList: yyy, workflowCount: totalRows, params: params])
            }
        } else {
            Workflow sss = new Workflow(yyy[0])
            println " 3>>>>>>>>>>>>>>>>>>>>>>>>>>>>>  " + sss.getWorkflowId()
            render(view: "index", model: [workflowList: yyy, workflowCount: totalRows, params: params])
        }
    }
}
