package com.b2b.ehubportal

import grails.converters.JSON
import org.grails.web.json.JSONElement

class DocumentContentController {

    private static final String DEFAULT_CHARSET = "UTF-8";
    DocumentContentService DocumentContentService

    def delete() {
        JSONElement jsonElement;

        def db = params.getRequest().getParameter('db')
        def filter = params.getRequest().getParameter('filter')

        db = db == null ? Enums.apiParamDb.ehub_aeon.toString() : db
        filter = 'docid::2201941' + "|version!::1"

        def www = documentContentService.deleteDocumentContent(db,filter)
        jsonElement = JSON.parse(www.getInputStream(), DEFAULT_CHARSET)
        def xxx = (Result) jsonElement

        println "DC RESPONSE CODE: "+ xxx.getResult().getCode()

        if (xxx.getResult().getCode()==200){
            println "Delete successfully"
        }
        else {
            println "Fail to delete"
        }
    }
}
