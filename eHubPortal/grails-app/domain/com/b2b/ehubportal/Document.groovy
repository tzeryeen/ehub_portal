package com.b2b.ehubportal

class Document {
    int docId
    int parentId
    String docNo
    int docSize
    Date lastActionDate = new Date()
    int status
    String docFileName
    String senderMailboxId
    String receiverMailboxId
    String description
    String creationDate
    int version
    String createdBy
    String lastActionBy
    String lastAction
    Date sentDate = new Date()
    Date receiveDate=new Date()
    String senderCode
    String receiverCode
    String startTime
    int errorFlag
    int lastErrorTypeId
    int modifyVersion
    String windowSize
    String metadata
    String docTypeId
    String docFormat
    int internal_status
    int retryCount_short
    String oriDocFileName
    String direction
    String state
    int correlationId
    String agreementId
    String workflowId
    Date updateDateTime=new Date()
    Date documentDate=new Date()
    int documentStatus
    String previous_state
    int latest_sequenceOrder
    String last_successful_state
    int retryInterval_short
    String organizationId
    String split_parentChild
    String senderOrgId
    String receiverOrgId
    String forwardCorrelationId
    String lastErrorDateTime
    int lastErrorId
    String jbossId
    int retryCount_long
    int retryInterval_long
    int retryInterval
    int remainingRetry
    int priority
    String ves_transmissionId
    String timeStamp
    String control_number
    int queue_priority
    int rowNum

    static constraints = {
    }
}
