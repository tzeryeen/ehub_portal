package com.b2b.ehubportal

class WorkflowProcess {

    String workflowID
    String processor_From
    String processor_To
    String processorSettingXml
    String type
    String rank
    int sequenceOrder
    int isBranch
    int isCheckFileMonitor
    String emonSenderId
    String emonReceiverId
    String clientType
    int point
    int rowNum

    static constraints = {
    }
}
