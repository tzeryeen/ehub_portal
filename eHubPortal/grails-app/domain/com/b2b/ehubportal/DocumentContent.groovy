package com.b2b.ehubportal

class DocumentContent {
    String content
    String timeStamp
    String activity
    String compressedFormat
    int compressedDocSize
    int docSize
    String docFormat
    String docContentFormat
    int contentId
    String filePath
    int rowNum

    static constraints = {
    }
}
