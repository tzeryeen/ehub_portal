package com.b2b.ehubportal

class Workflow {
    String workflowId
    String workflowName
    String workflowDescription
    String status
    Date activationDate = new Date()
    int priority
    int noDocBatch
    String maxAllowedThroughput
    String senderMailboxId
    String ownerId
    String receiverMailboxId
    String docTypeId
    int retryIntervalShort
    String duplicateFlow
    String allowCreate
    String allowImport
    int retryIntervalLong
    String startTime
    String windowSize
    String delayInterval
    String retryInterval
    int workflowFeature
    int rowNum

    static constraints = {
    }
}
