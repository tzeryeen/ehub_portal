package com.b2b.ehubportal

class DocumentContentMapping {

    int docId
    int contentId
    String comm_sequence
    int version1
    String documentTypeId
    int contentTypeId
    int rowNum

    static constraints = {
    }

}
