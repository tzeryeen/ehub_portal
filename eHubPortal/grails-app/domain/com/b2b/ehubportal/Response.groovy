package com.b2b.ehubportal

class Response {
    String code
    String status
    String message
    Map[] data

    static constraints = {
    }
}
