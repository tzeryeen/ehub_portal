package com.b2b.ehubportal

class Agreement {
    String agreementId
    int status
    Date activationEndDate = new Date()
    Date  activationStartDate = new Date()
    String workflowId
    String organizationId
    String documentTypeId
    String receiverMailbox
    String senderMailbox
    int version
    String updateDateTime
    String name
    String description
    String direction
    int priority
    String emonSenderId
    String emonReceiverId
    int queuePriority
    int rownum

    static constraints = {
    }
}
