<!doctype html>
<html>
<head>
    <title>Page Not Found</title>
    <meta name="layout" content="main">
    <g:if env="development"><asset:stylesheet src="errors.css"/></g:if>
</head>

<body>
<main role="main" class="col-md-9 ml-sm-auto col-lg-10 pt-3 px-4">
    <ul class="errors">
        <li>Error: Page Not Found (404)</li>
        <li>Path: ${request.forwardURI}</li>
    </ul>
</main>
</body>
</html>
