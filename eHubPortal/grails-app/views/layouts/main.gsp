<%@ page import="com.b2b.ehubportal.Enums" %>

<!doctype html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <title>
        <g:layoutTitle default="Home"/>
    </title>

    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <asset:stylesheet src="bootstrap.min.css"/>
    <asset:stylesheet src="dashboard.css"/>

    <g:layoutHead/>
</head>
<body>

<!-- Header Menu -->
<nav class="navbar fixed-top navbar-dark bg-dark flex-md-nowrap p-0">
    <a class="navbar-brand col-sm-3 col-md-2" href="#">eHub Portal</a>
    <div class="dropdown px-0">
        <ul class="nav text-light">
            <li class="dropdown nav-item">
                Database :   <g:select id="eHubDb" name="eHubDb" from="${com.b2b.ehubportal.Enums.apiParamDb}" required=""/>
            </li>
        </ul>
    </div>
    <div class="dropdown px-3 w-50">
        <ul class="nav">.</ul>
    </div>
    <div class="dropdown px-3">
        <ul class="nav">
            <li class="dropdown nav-item"><a href="#" class="nav-link text-light" data-toggle="dropdown">XXXX XXXX XXXX </a>
                <ul class="dropdown-menu py-0">
                    <li class="nav-item"><a class="nav-link py-1" href="#"><span data-feather="settings"></span>  Preferences</a></li>
                    <li class="nav-item"><a class="nav-link py-1" href="#"><span data-feather="log-out"></span>  Logout</a></li>
                </ul>
            </li>
        </ul>
    </div>
</nav>

<div class="container-fluid">
    <div class="row row-offcanvas row-offcanvas-left">
        <!-- Side Menu -->
        <div class="sidebar-sticky col-md-3 col-lg-2 sidebar-offcanvas pl-0 bg-light sidebar" id="sidebar">
            <div class="list-group panel">
                <ul class="nav flex-column">
                    <a href="/home" class="nav-link list-group-item" data-parent="#sidebar" id="dashboard"><span data-feather="home"></span>Dashboard</a>
                </ul>

                <a href="#menu1" class="nav-link list-group-item" data-toggle="collapse" data-parent="#sidebar"><span data-feather="file"></span>Documents <i class="fa fa-caret-down"></i></a>
                <div class="collapse" id="menu1">
                    <ul class="flex-column pl-2 nav">
                        <a href="/document/index" class="nav-link list-group-item py-2" data-parent="#menu1" id="doc_search"><span data-feather="search"></span>Search</a>
                        <a href="/document/update" class="nav-link list-group-item py-2" data-parent="#menu1" id="doc_reprocess"><span data-feather="activity"></span>Reprocess</a>
                    </ul>
                </div>

                <h6 class="sidebar-heading d-flex justify-content-between align-items-center px-3 mt-4 mb-1 text-muted">
                    <span>eHub</span>
                    <a class="d-flex align-items-center text-muted" href="#"></a>
                </h6>
                <a href="#menu2" class="nav-link list-group-item disabled" data-toggle="collapse" data-parent="#sidebar"><span data-feather="link"></span>Flow</a>
                <div class="collapse" id="menu2">
                    <ul class="flex-column pl-2 nav">
                        <a href="/workflow" class="nav-link list-group-item py-2" data-parent="#menu2" id="flow_workflow"><span data-feather="tag"></span>Workflow</a>
                        <a href="#" class="nav-link list-group-item disabled py-2" data-parent="#menu2" id="flow_agreement"><span data-feather="tag"></span>Agreement</a>
                        <a href="#" class="nav-link list-group-item disabled py-2" data-parent="#menu2" id="flow_workflowprocess"><span data-feather="tag"></span>Workflow Process</a>
                    </ul>
                </div>

                <a href="#menu3" class="nav-link list-group-item disabled" data-toggle="collapse" data-parent="#sidebar"><span data-feather="inbox"></span>File</a>
                <div class="collapse" id="menu3">
                    <ul class="flex-column pl-2 nav">
                        <a href="#" class="nav-link list-group-item disabled py-2" data-parent="#menu3" id="file_document"><span data-feather="file"></span>Document</a>
                        <a href="#" class="nav-link list-group-item disabled py-2" data-parent="#menu3" id="file_doc_content_mapping"><span data-feather="file"></span>Document Content Mapping</a>
                        <a href="#" class="nav-link list-group-item disabled py-2" data-parent="#menu3" id="file_doc_confirmation"><span data-feather="file"></span>Document Confirmation</a>
                        <a href="#" class="nav-link list-group-item disabled py-2" data-parent="#menu3" id="file_doc_monitoring"><span data-feather="file"></span>Document Monitoring</a>
                        <a href="#" class="nav-link list-group-item disabled py-2" data-parent="#menu3" id="file_doc_tracking"><span data-feather="file"></span>Document Tracking</a>
                        <a href="#" class="nav-link list-group-item disabled py-2" data-parent="#menu3" id="file_split_docs"><span data-feather="file"></span>Split Docs</a>
                        <a href="#" class="nav-link list-group-item disabled py-2" data-parent="#menu3" id="file_merged_docs"><span data-feather="file"></span>Merged Docs</a>
                    </ul>
                </div>
                <a href="#menu4" class="nav-link list-group-item disabled" data-toggle="collapse" data-parent="#sidebar"><span data-feather="layers"></span>Others</a>
                <div class="collapse" id="menu4">
                    <ul class="flex-column pl-2 nav">
                        <a href="#" class="nav-link list-group-item disabled py-2" data-parent="#menu4" id="oth_doc_type"><span data-feather="bookmark"></span>Document Type</a>
                        <a href="#" class="nav-link list-group-item disabled py-2" data-parent="#menu4" id="oth_mailbox"><span data-feather="bookmark"></span>Mailbox</a>
                        <a href="#" class="nav-link list-group-item disabled py-2" data-parent="#menu4" id="oth_ehub_client"><span data-feather="bookmark"></span>eHub Client</a>
                        <a href="#" class="nav-link list-group-item disabled py-2" data-parent="#menu4" id="oth_ehub_client_config"><span data-feather="bookmark"></span>eHub Client Config</a>
                        <a href="#" class="nav-link list-group-item disabled py-2" data-parent="#menu4" id="oth_mailbox_ehub_client"><span data-feather="bookmark"></span>Mailbox eHub Client</a>
                        <a href="#" class="nav-link list-group-item disabled py-2" data-parent="#menu4" id="oth_processor"><span data-feather="bookmark"></span>Processor</a>
                        <a href="#" class="nav-link list-group-item disabled py-2" data-parent="#menu4" id="oth_id_seq_generator"><span data-feather="bookmark"></span>ID Sequence Generator</a>
                        <a href="#" class="nav-link list-group-item disabled py-2" data-parent="#menu4" id="oth_admin_setting"><span data-feather="bookmark"></span>Admin Setting</a>
                        <a href="#" class="nav-link list-group-item disabled py-2" data-parent="#menu4" id="oth_client_identification"><span data-feather="bookmark"></span>Client Identification</a>
                    </ul>
                </div>

                <h6 class="sidebar-heading d-flex justify-content-between align-items-center px-3 mt-4 mb-1 text-muted">
                    <span>Configuration</span>
                    <a class="d-flex align-items-center text-muted" href="#"></a>
                </h6>
                <ul class="nav flex-column">
                    <a href="#" class="nav-link list-group-item disabled" data-parent="#sidebar" id="setup_workflow"><span data-feather="navigation"></span>Setup Workflow</a>
                    <a href="#" class="nav-link list-group-item disabled" data-parent="#sidebar" id="setup_econnector"><span data-feather="box"></span>Setup eConnector</a>
                    <a href="#" class="nav-link list-group-item disabled" data-parent="#sidebar" id="block_release"><span data-feather="power"></span>Block/Release Flow</a>
                </ul>
            </div>
        </div>
        <g:layoutBody/>
    </div>
</div>

<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<asset:javascript src="jquery-3.2.1.min.js"/>
<asset:javascript src="popper.min.js"/>
<asset:javascript src="bootstrap.min.js"/>

<!-- Icons -->
<script src="https://unpkg.com/feather-icons/dist/feather.min.js"></script>
<script>
    feather.replace();
    $(document).ready(function () {
        // to cater the selected click in menu
        $(".nav-link").click(function () {
            var id = $(this).attr("id");
            $('a .nav-link').find(".active").removeClass("active");
            $(id).addClass("active");
            localStorage.setItem("selectedolditem", id);

        });

        var selectedolditem = localStorage.getItem('selectedolditem');
        if (selectedolditem !== null) {
            $('#' + selectedolditem).addClass("active");
        }

        // to cater the expanded menu after postback
        $("#sidebar div .collapse").on('shown.bs.collapse', function() {
            var activeDetails = $("#sidebar div .show").attr('id');
            sessionStorage.setItem('activeDetailsGroup', activeDetails);
        });
        $("#sidebar div .collapse").on('hidden.bs.collapse', function() {
            $("#sidebar div .collapse").removeClass('show');
            sessionStorage.removeItem('activeDetailsGroup');
        });
        var lastDetails = sessionStorage.getItem('activeDetailsGroup');
        if (lastDetails !== null) {
            //remove default collapse settings
            $("#sidebar div .collapse").removeClass('show');
            //show the account_last visible group
            $("#" + lastDetails).addClass("show");
        }
    });
</script>
</body>
</html>
