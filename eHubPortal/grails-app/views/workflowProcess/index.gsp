<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="layout" content="main"/>

    <title><g:message code="workflow.label"/></title>
</head>

<body>

<main role="main" class="col-md-9 ml-sm-auto col-lg-10 pt-3 px-4">
    <div class="pt-5 pb-2 mb-3">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="/home">Home</a></li>
                <li class="breadcrumb-item active">eHub</li>
                <li class="breadcrumb-item active">Flow</li>
                <li class="breadcrumb-item active" aria-current="page"><g:message code="workflow.label" /></li>
            </ol>
        </nav>
        <div class="d-flex align-items-center border-bottom">
            <h3 class="d-none d-sm-block">Workflow Process</h3>
        </div>
    </div>

    <div class="col-lg-auto col-md-auto">
        <div class="table-responsive">
            <table class="table table-hover">
                <thead>
                <tr>
                    <th scope="col"><sup><g:message code="workflow.workflowId.label" /></sup></th>
                    <th scope="col"><sup><g:message code="workflowprocess.sequenceOrder.label" /></sup></th>
                    <th scope="col"><sup><g:message code="workflowprocess.processor_From.label" /></sup></th>
                    <th scope="col"><sup><g:message code="workflowprocess.processor_To.label" /></sup></th>
                    <th scope="col"><sup><g:message code="workflowprocess.type.label" /></sup></th>
                    <th scope="col"><sup><g:message code="workflowprocess.processorSettingXml.label" /></sup></th>
                </tr>
                </thead>
                <tbody>
                <g:each status="i" in="${workflowProcessList}" var="workflowprocess">
                    <tr>
                        <td><small>${workflowprocess.workflowID}</small></td>
                        <td><small>${workflowprocess.sequenceOrder}</small></td>
                        <td><small>${workflowprocess.processor_From}</small></td>
                        <td><small>${workflowprocess.processor_To}</small></td>
                        <td><small>${workflowprocess.type}</small></td>
                        <td><small>${workflowprocess.processorSettingXml}</small></td>
                    </tr>
                </g:each>
                </tbody>
            </table>
        </div>
        <g:paginate controller="workflowProcess" action="list" total="${workflowProcessCount}" params="${params}"/>
    </div>

</main>
</body>
</html>