<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="layout" content="main"/>
    <title><g:message code="document.label" /></title>
</head>

<body>
<main role="main" class="col-md-9 ml-sm-auto col-lg-10 pt-3 px-4">
    <div class="pt-5 pb-2 mb-3">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="/home">Home</a></li>
                <li class="breadcrumb-item active">eHub</li>
                <li class="breadcrumb-item active">Flow</li>
                <li class="breadcrumb-item active" aria-current="page"><g:message code="document.label" /></li>
            </ol>
        </nav>
            <div class="d-flex align-items-center border-bottom">
                <h3 class="d-none d-sm-block">Document</h3>
            </div>
    </div>

    <div class="container">
        <form action="displayUpdate"  controller="document">
            <table class="table table-hover">
                <tr>
                    <td>
                        <div class="col-xs-3">
                            <label for="docID">DOCID:</label><br>
                            <input type="text" class="form-control-sm" id="docID" placeholder="Enter Docid"
                                   name="docID">
                        </div>
                        <div class="col-xs-3">
                            <label for="docTypeId">DOCTYPEID:</label><br>
                            <input type="text" class="form-control-sm" id="docTypeId" placeholder="Enter Doctypeid"
                                   name="docTypeId">
                        </div>
                        <div class="col-xs-3">
                            <label for="status">STATUS:</label><br>
                            <input type="text" class="form-control-sm" id="status" placeholder="Enter Status" name="status">
                        </div>
                        <div class="col-xs-3">
                            <label for="status">FROM_DATE:</label><br>
                            <input type="date" class="form-control-sm" id="creationFromDate" placeholder="Enter From Date" name="creationFromDate">
                        </div>
                    </td>
                    <td>
                        <div class="col-xs-3">
                            <label for="status">SENDERMAILBOXID:</label><br>
                            <input type="text" class="form-control-sm" id="senderMailboxId"
                                   placeholder="Enter Sendermailboxid" name="senderMailboxId">
                        </div>
                        <div class="col-xs-3">
                            <label for="receiverMailboxId">RECEIVERMAILBOXID:</label><br>
                            <input type="text" class="form-control-sm" id="receiverMailboxId"
                                   placeholder="Enter Receivermailboxid" name="receiverMailboxId">
                        </div>
                        <div class="col-xs-3">
                            <label for="docFileName">DOCFILENAME:</label><br>
                            <input type="text" class="form-control-sm" id="docFileName" placeholder="Enter Docfilename"
                                   name="docFileName">
                        </div>
                        <div class="col-xs-3">
                            <label for="docFileName">TO_DATE:</label><br>
                            <input type="date" class="form-control-sm" id="creationToDate" placeholder="Enter To Date" name="creationToDate">
                        </div>
                    </td>
                </tr>
            </table>
            <br>
            <button class="btn-group-sm">SEARCH</button>
        </form>
    </div>

    <div class="col-lg-auto col-md-auto">
        <div class="table-responsive">
            <table class="table table-hover">

            <thead>
            <tr>
                <th scope="col"></th>
                <th scope="col">#</th>
                <th scope="col"><sup><g:message code="document.docid.label"/></sup></th>
                <th scope="col"><sup><g:message code="document.status.label" /></sup></th>
                <th scope="col"><sup><g:message code="document.docfilename.label" /></sup></th>
                <th scope="col"><sup><g:message code="document.doctype.label" /></sup></th>
                <th scope="col"><sup><g:message code="document.sendermailboxid.label" /></sup></th>
                <th scope="col"><sup><g:message code="document.receiverMailboxId.label" /></sup></th>
                <th scope="col"><sup><g:message code="document.version.label" /></sup></th>
                <th scope="col"><sup><g:message code="document.creationdate.label" /></sup></th>
                <th scope="col"><sup><g:message code="document.symbol" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</sup></th>
            </tr>
            </thead>

            <tbody>
            <br/>
            <g:form action="reprocess" controller="document"> <button>REPROCESS</button>
                <g:each status="i" in="${documentSearch}" var="document">
                    <tr>
                        <th><input type="checkbox" name="checklist" value="${document.docId}"/></th>
                        <th scope="row">${document.rowNum}</th>
                        <td><small>${document.docId}</small></td>
                        <td><small>${document.status}</small></td>
                        <td><small>${document.docFileName}</small></td>
                        <td><small>${document.docTypeId}</small></td>
                        <td><small>${document.senderMailboxId}</small></td>
                        <td><small>${document.receiverMailboxId}</small></td>
                        <td><small>${document.version}</small></td>
                        <td><small>${document.creationDate}</small></td>


                        <td>
                            <g:link action="metadata" controller="document" params="${[categoryName: document.docId]}"><asset:image src="LastImages.jpg" width="20" height="20" /></g:link>
                            <g:link action="search" controller="workflowProcess" params="${[filter: "workflowId::" +  document.workflowId]}"><asset:image src="ImagesViews.jpg" width="20" height="20" /></g:link>
                            <g:link action="search" controller="documentContentMapping" params="${[filter: "docId::" + document.docId]}"><asset:image src="EyeView.png" width="20" height="20" /></g:link></td>
                    </tr>
                </g:each>
            </g:form>
            </tbody>
            </table>
            <div>
                <g:paginate controller="document" action="displayUpdate" total="${documentCount}" params="${params}"/>
            </div>
            </div>
    </div>
</main>
</body>
</html>
