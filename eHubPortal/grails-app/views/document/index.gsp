<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="layout" content="main"/>
    <title><g:message code="document.label"/></title>
</head>

<body>

<main role="main" class="col-md-9 ml-sm-auto col-lg-10 pt-3 px-4">
    <div class="pt-5 pb-2 mb-3">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="/home">Home</a></li>
                <li class="breadcrumb-item active">eHub</li>
                <li class="breadcrumb-item active">Flow</li>
                <li class="breadcrumb-item active" aria-current="page"><g:message code="document.label"/></li>
            </ol>
        </nav>

        <div class="d-flex align-items-center border-bottom">
            <h3 class="d-none d-sm-block">Document</h3>
        </div>
    </div>


    <div class="container">
        <form action="searchDoc" controller="document">
            <table class="table table-hover">
                <tr>
                    <td>
                        <div class="col-xs-3">
                            <label for="docID">DOCID:</label><br>
                            <input type="text" class="form-control-sm" id="docID" placeholder="Enter Docid"
                                   name="docID">
                        </div>
                        <div class="col-xs-3">
                            <label for="docTypeId">DOCTYPEID:</label><br>
                            <input type="text" class="form-control-sm" id="docTypeId" placeholder="Enter Doctypeid"
                                   name="docTypeId">
                        </div>
                        <div class="col-xs-3">
                            <label for="status">STATUS:</label><br>
                            <input type="text" class="form-control-sm" id="status" placeholder="Enter Status" name="status">
                        </div>
                        <div class="col-xs-3">
                            <label for="creationFromDate">FROM_DATE:</label><br>
                            <input type="date" class="form-control-sm" id="creationFromDate" placeholder="Enter From Date" name="creationFromDate">
                        </div>
                    </td>
                    <td>
                        <div class="col-xs-3">
                            <label for="status">SENDERMAILBOXID:</label><br>
                            <input type="text" class="form-control-sm" id="senderMailboxId"
                                   placeholder="Enter Sendermailboxid" name="senderMailboxId">
                        </div>
                        <div class="col-xs-3">
                            <label for="receiverMailboxId">RECEIVERMAILBOXID:</label><br>
                            <input type="text" class="form-control-sm" id="receiverMailboxId"
                                   placeholder="Enter Receivermailboxid" name="receiverMailboxId">
                        </div>
                        <div class="col-xs-3">
                            <label for="docFileName">DOCFILENAME:</label><br>
                            <input type="text" class="form-control-sm" id="docFileName" placeholder="Enter Docfilename"
                                   name="docFileName">
                        </div>
                        <div class="col-xs-3">
                            <label for="creationToDate">TO_DATE:</label><br>
                            <input type="date" class="form-control-sm" id="creationToDate" placeholder="Enter To Date" name="creationToDate">
                        </div>
                    </td>
                </tr>
            </table>
            <button class="btn-group-sm">SEARCH</button>
        </form>
    </div>
</main>
</body>
</html>



