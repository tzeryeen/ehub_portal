<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="layout" content="main"/>

    <title><g:message code="document.label" /></title>
</head>

<body>
<main role="main" class="col-md-9 ml-sm-auto col-lg-10 pt-3 px-4">
    <div class="pt-5 pb-2 mb-3">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="/home">Home</a></li>
                <li class="breadcrumb-item active">eHub</li>
                <li class="breadcrumb-item active">Flow</li>
                <li class="breadcrumb-item active" aria-current="page"><g:message code="document.label" /></li>
            </ol>
        </nav>
        <div class="d-flex align-items-center border-bottom">
            <h3 class="d-none d-sm-block">Document</h3>
        </div>
    </div>

    <div class="col-lg-auto col-md-auto">
        <div class="table-responsive">
            <table class="table table-hover">
                <thead>
                <tr>

                    <th scope="col">#</th>
                    <th scope="col"><sup><g:message code="document.docid.label"/></sup></th>
                    <th scope="col"><sup><g:message code="document.status.label" /></sup></th>
                    <th scope="col"><sup><g:message code="document.docfilename.label" /></sup></th>
                    <th scope="col"><sup><g:message code="document.doctype.label" /></sup></th>
                    <th scope="col"><sup><g:message code="document.sendermailboxid.label" /></sup></th>
                    <th scope="col"><sup><g:message code="document.receiverMailboxId.label" /></sup></th>
                    <th scope="col"><sup><g:message code="document.version.label" /></sup></th>
                    <th scope="col"><sup><g:message code="document.creationdate.label" /></sup></th>

                </tr>
                </thead>
                <tbody>
                <g:each status="i" in="${documentList}" var="document">
                    <tr>

                        <th scope="row">${document.rowNum}</th>
                        <td><small>${document.docId}</small></td>
                        <td><small>${document.status}</small></td>
                        <td><small>${document.docFileName}</small></td>
                        <td><small>${document.docTypeId}</small></td>
                        <td><small>${document.senderMailboxId}</small></td>
                        <td><small>${document.receiverMailboxId}</small></td>
                        <td><small>${document.version}</small></td>
                        <td><small>${document.creationDate}</small></td>

                    </tr>
                </g:each>
                </tbody>
            </table>
        </div>

        <g:paginate controller="document" action="list" total="${documentCount}" params="${params}"/>
    </div>
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <asset:javascript src="jquery-3.2.1.min.js"/>
    <asset:javascript src="popper.min.js"/>
    <asset:javascript src="bootstrap.min.js"/>
</main>
</body>
</html>
