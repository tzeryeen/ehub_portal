<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="layout" content="main"/>

    <title><g:message code="metadata.label" /></title>
</head>

<body>
<main role="main" class="col-md-9 ml-sm-auto col-lg-10 pt-3 px-4">
    <div class="pt-5 pb-2 mb-3">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="/home">Home</a></li>
                <li class="breadcrumb-item active">eHub</li>
                <li class="breadcrumb-item active">Flow</li>
                <li class="breadcrumb-item active">Document</li>
                <li class="breadcrumb-item active" aria-current="page"><g:message code="metadata.label" /></li>
            </ol>
        </nav>
        <div class="d-flex align-items-center border-bottom">
            <h3 class="d-none d-sm-block">Metadata</h3>
        </div>
    </div>

    <div class="col-lg-auto col-md-auto">
        <div class="table-responsive">

                <g:each status="i" in="${documentSearch}" var="document">

                    <textarea class="form-control"  style="min-width: 100%" rows="10" disabled="disabled">${document.metadata}</textarea>

                </g:each>
                </tbody>

        </div>


    </div>
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <asset:javascript src="jquery-3.2.1.min.js"/>
    <asset:javascript src="popper.min.js"/>
    <asset:javascript src="bootstrap.min.js"/>
</main>
</body>
</html>
