<!doctype html>

<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="layout" content="main"/>

    <title><g:message code="docCM.label" /></title>
</head>

<body>
<main role="main" class="col-md-9 ml-sm-auto col-lg-10 pt-3 px-4">
    <div class="pt-5 pb-2 mb-3">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="/home">Home</a></li>
                <li class="breadcrumb-item active">eHub</li>
                <li class="breadcrumb-item active">Flow</li>
                <li class="breadcrumb-item active">DCM</li>
                <li class="breadcrumb-item active" aria-current="page">View</li>
            </ol>
        </nav>

        <g:textArea name="myField" value="${xmlString}" rows="24" cols="150" disabled="true"/>
        <br/>

    </div>

</main>
</body>


<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<asset:javascript src="jquery-3.2.1.min.js"/>
<asset:javascript src="popper.min.js"/>
<asset:javascript src="bootstrap.min.js"/>
</html>