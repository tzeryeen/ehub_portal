<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="layout" content="main"/>

    <title><g:message code="docCM.label"/></title>

</head>

<body>
<main role="main" class="col-md-9 ml-sm-auto col-lg-10 pt-3 px-4">
    <div class="pt-5 pb-2 mb-3">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="/home">Home</a></li>
                <li class="breadcrumb-item active">eHub</li>
                <li class="breadcrumb-item active">Flow</li>
                <li class="breadcrumb-item active">Document</li>
                <li class="breadcrumb-item active" aria-current="page">DCM</li>
            </ol>
        </nav>
        <div class="d-flex align-items-center border-bottom">
            <h3 class="d-none d-sm-block">Document Content Mapping :: ${docId}</h3>
        </div>
    </div>

    <div class="col-lg-auto col-md-auto">
        <div class="table-responsive">
            <table class="table table-hover">
                <thead>
                <tr>
                    <th scope="col"><g:message code="docCM.version.label"/></th>
                    <th scope="col"><g:message code="docCM.contentId.label"/></th>
                    <th scope="col"><g:message code="docCM.viewDownload.label"/></th>
                </tr>
                </thead>
                <tbody>
                <g:each status="i" in="${dcmList}" var="dcm">
                    <tr>
                        <td>${dcm.version1}</td>
                        <td>${dcm.contentId}</td>
                        <td>
                            <g:link controller="mongo" action="view" params="[contentId: dcm.contentId]">
                                <span style="width:60px;height:10px"><button type="button" class="btn btn-info">View</button></span>
                            </g:link>
                            <g:link controller="mongo" action="download" params="[contentId: dcm.contentId]">
                                <span style="width:100px;height:10px"><button type="button" class="btn btn-success">Download</button></span>
                            </g:link></td>
                    </tr>
                </g:each>
                </tbody>
            </table>
        </div>
    </div>
</main>
</body>
</html>